<?php
include 'index.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $article_id = mysqli_real_escape_string($conn, $_POST['article_id']);
    $user_id = mysqli_real_escape_string($conn, $_POST['user_id']);

    $sql = "SELECT * FROM cart WHERE user_id = $user_id AND article_id = $article_id";
    $result = mysqli_query($conn, $sql);
    $new_cart_id = mysqli_fetch_row(mysqli_query($conn, "SELECT MAX(cart_id) FROM cart WHERE user_id = $user_id"))[0] + 1;
    $sql = "INSERT INTO cart (user_id, article_id, cart_id) VALUES ($user_id, $article_id, $new_cart_id)";


    if (mysqli_query($conn, $sql)) {
        echo "L'article a été ajouté au panier avec succès.";
    } else {
        echo "Erreur lors de l'ajout de l'article au panier : " . mysqli_error($conn);
    }

    mysqli_close($conn);

    // Rediriger l'utilisateur vers la page "cart.php"
    header("Location: home.php");
    exit();
}
?>
