# php_exam

### ➜ **php files**

- [account.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/account.php)
- [add_cart.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/add_cart.php)
- [add_cart2.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/add_cart2.php)
- [admin.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/admin.php)
- [ajouter_solde.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/ajouter_solde.php)
- [articles.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/articles.php)
- [cart.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/cart.php)
- [delete_article.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/delete_article.php)
- [delete_cart.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/delete_cart.php)
- [detail.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/detail.php)
- [edit_account.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/edit_account.php)
- [edit_article.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/edit_article.php)
- [edit.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/edit.php)
- [home.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/home.php)
- [index.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/index.php)
- [login.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/login.php)
- [register.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/register.php)
- [sell.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/sell.php)
- [soustraire_solde.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/soustraire_solde.php)
- [suppr.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/suppr.php)
- [validate.php](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/validate.php)

### ➜ **css files**

- [admin.css](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/admin.css)
- [cart.css](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/cart.css)
- [detail.css](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/detail.css)
- [edit.css](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/edit.css)
- [home.css](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/home.css)
- [index.css](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/index.css)
- [sell.css](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/sell.css)
- [validate.css](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/validate.css)

### ➜ **DB**

- [php_exam_db.sql](https://gitlab.com/MartinREVOL/php_exam/-/blob/main/php_exam_db.sql)

![grr](https://static.fnac-static.com/multimedia/Images/FR/NR/4c/e9/34/3467596/1540-1/tsp20140224103919/Grr-mes-p-tits-copains-de-la-ferme.jpg)