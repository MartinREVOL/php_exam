<?php
include 'index.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $article_id = mysqli_real_escape_string($conn, $_POST['article_id']);

    // Supprimer l'article de la table "stock"
    $sql2 = "DELETE FROM stock WHERE article_id = $article_id";
    if (mysqli_query($conn, $sql2)) {
        echo "Les données du stock ont été supprimées avec succès.";
    } else {
        echo "Erreur lors de la suppression des données du stock : " . mysqli_error($conn);
    }

    // Supprimer l'article de la table "article"
    $sql1 = "DELETE FROM article WHERE article_id = $article_id";
    if (mysqli_query($conn, $sql1)) {
        echo "L'article a été supprimé avec succès.";
    } else {
        echo "Erreur lors de la suppression de l'article : " . mysqli_error($conn);
    }

    mysqli_close($conn);

    // Rediriger l'utilisateur vers la page "articles.php"
    header("Location: articles.php");
    exit();
}
?>