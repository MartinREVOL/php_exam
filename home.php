<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="home.css">
  <title>Home</title>
</head>
<body>
  <!-- Navbar -->
  <div id="navbar">
  <div id="navbar-title">Accueil</div>
  <a href="articles.php">
    <i class="fa fa-fw fa-shopping-cart"></i>
    Articles
  </a>
  <a href="cart.php">
    <i class="fa fa-fw fa-shopping-basket"></i>
    Panier
  </a>
  <a href="account.php">
    <i class="fa fa-fw fa-user"></i>
    Profil
  </a>
</div>


  <!-- Page Content -->
  <div class="content">
    <h1>Articles en vente</h1>
      <?php
      include 'index.php';
      $query = "SELECT * FROM article ORDER BY pub_date DESC";
      $result = mysqli_query($conn, $query);
      if (mysqli_num_rows($result) > 0) {
          while ($row = mysqli_fetch_assoc($result)) {
      ?>
      <div class="card" onclick="location.href='detail.php?article_id=<?php echo $row['article_id']; ?>'">
        <img src="<?php echo $row['image_link']; ?>" alt="Article Image">
        <div class="card-content">
          <h3><?php echo $row['name']; ?></h3>
          <p class="price">Prix: <?php echo $row['cost']; ?>€</p>
        </div>
      </div>
      <?php
            }
        } else {
            echo "Aucun article en vente pour le moment.";
        }
      ?>
  </div>

</body>
</html>
