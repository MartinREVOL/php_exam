<?php if ($_COOKIE['id'] == null) {
    header("Location: login.php");
    exit();
}
session_start();
include 'index.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);

    $sql = "SELECT * FROM user WHERE e_mail = '$email'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $stored_password = $row['mdp'];

        if (password_verify($password, $stored_password)) {
            // Login successful, redirect to another page
            $id = $row['user_id'];
            setcookie('id', $id, time() + (60 * 60));
            header("Location: home.php");
            exit();
        } else {
            // Login failed, display an error message
            echo "Incorrect email or password, please try again.";
        }
    } else {
        // Login failed, display an error message
        echo "Incorrect email or password, please try again.";
    }
}
?>

<!DOCTYPE html>
    <head>
        <meta charset="UTF-8">
        <title>Cart</title>
        <link rel="stylesheet" href="home.css">
    </head>
    <body>
        <!-- Navbar -->
    <div id="navbar">
    <a href="home.php">
        <i class="fa fa-fw fa-shopping-basket"></i>
        Accueil
    </a>
    <a href="articles.php">
        <i class="fa fa-fw fa-shopping-cart"></i>
        Articles
    </a>
    <div id="navbar-title">Panier</div>
    <a href="account.php">
        <i class="fa fa-fw fa-user"></i>
        Profil
    </a>
    </div>

    <div class="content">
        <h1>Récap du panier</h1>
        <?php
            include 'index.php';
            $user_id = $_COOKIE['id'];
            $query = "SELECT cart.article_id as article_id, COUNT(cart.article_id) as nbr_article , article.cost as cost, article.name as name FROM cart JOIN article ON cart.article_id = article.article_id WHERE cart.user_id GROUP BY cart.article_id;";
            $query1 = "SELECT SUM(solde) as total FROM user WHERE user_id = $user_id ORDER BY user_id DESC LIMIT 1;";
            $query2 = "SELECT SUM(article.cost) as total FROM `cart` JOIN article ON cart.article_id = article.article_id GROUP BY cart.user_id;";
            $result = mysqli_query($conn, $query);
            $result1 = mysqli_query($conn, $query1);
            $result2 = mysqli_query($conn, $query2);
            $row1 = mysqli_fetch_assoc($result1);
            ?>
                <div class="solde">
                <h3 class="price">Solde: <?php echo $row1['total']; ?>€</h3>
                </div>
            <?php


        while ($row = mysqli_fetch_assoc($result)) {
                    ?>
                    <div class="article">
                      <div class="article_price">
                        <h3 class="name"><?php echo $row['name']; ?></h3>
                        <p class="price">Prix: <?php echo $row['cost']; ?>€</p>
                        <p class="Nbr_article">Exemplaire: <?php echo $row['nbr_article'];?></p>
                      </div>
                    </div> 
                    <?php
        }
        $row2 = mysqli_fetch_assoc($result2);
        if ($row2['total'] <= $row1['total']) {
            ?>
            <div class="commande">
            <form action="" method="post">
                <label for="fct_adresse">Adresse :</label>
                <input type="fct_adresse" id="fct_adresse" name="fct_adresse" required>
                <label for="fct_city">Ville :</label>
                <input type="fct_city" id="fct_city" name="fct_city" required>
                <label for="fct_cp">Code Postal :</label>
                <input type="fct_cp" id="fct_cp" name="fct_cp" required>
                <input type="submit" value="Done">
            </form>
            <h3><a href="validate.php">Valider La Commande</a></h3>
            </div>
            <?php
        } else {
            ?>
            <h3>Vous ne pouvez pas passer commande votre solde est insuffisant</h3>
            <?php
        }
        ?>
    </div>
    </body>
</html>