<?php
include 'index.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Vérifier si la variable est définie
    if (isset($_POST['article_id'])) {
        $article_id = $_POST['article_id'];

        // Créer une requête préparée
        $sql2 = "DELETE FROM cart WHERE article_id = ? LIMIT 1";
        $stmt = mysqli_prepare($conn, $sql2);
        
        if ($stmt) {
            // Lier les variables à la requête préparée
            mysqli_stmt_bind_param($stmt, "i", $article_id);

            // Exécuter la requête
            if (mysqli_stmt_execute($stmt)) {
                echo "Les données du panier ont été supprimées avec succès.";
            } else {
                echo "Erreur lors de la suppression des données du panier : " . mysqli_error($conn);
            }

            // Fermer la requête
            mysqli_stmt_close($stmt);
        } else {
            echo "Erreur lors de la création de la requête préparée : " . mysqli_error($conn);
        }

        // Fermer la connexion
        mysqli_close($conn);

        // Rediriger l'utilisateur vers la page "cart.php"
        header("Location: cart.php");
        exit();
    }
}
?>
