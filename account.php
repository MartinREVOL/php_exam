<?php if ($_COOKIE['id'] == null) {
    header("Location: login.php");
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="home.css">
  <title>Home</title>
</head>
<body>
  <!-- Navbar -->
  <div id="navbar">
  <a href="home.php">
    <i class="fa fa-fw fa-user"></i>
    Accueil
  </a>
  <a href="articles.php">
    <i class="fa fa-fw fa-shopping-cart"></i>
    Articles
  </a>
  <a href="cart.php">
    <i class="fa fa-fw fa-shopping-basket"></i>
    Panier
  </a>
  <div id="navbar-title">Profil</div>
</div>


  <!-- Page Content -->
  <div class="content">
    <h1>Mon profil</h1>
    <?php
      include 'index.php';
      $users_id = $_COOKIE['id'];
      $query = "SELECT * FROM user WHERE user_id = $users_id";
      $result = mysqli_query($conn, $query);
      if (mysqli_num_rows($result) > 0) {
          while ($row = mysqli_fetch_assoc($result)) {

      ?>
    <div class="profile">
  <div class="profile-picture"><img src="<?php echo $row['pp']; ?>" alt="Profil Image"></div>
  <div class="profile-info">
    <p>Pseudo : <?php echo $row['username']; ?></p>
    <p>Solde : <?php echo $row['solde']; ?> <form action="ajouter_solde.php" method="post">
  <input type="submit" name="plus" value="+">
</form><form action="soustraire_solde.php" method="post">
  <input type="submit" name="moins" value="-">
</form></p>
  </div>
  <a href="edit_account.php">
  <button>Modifier le profil</button>
  </a>

  <?php
            }
        }
      ?>
</div>

  </div>

</body>
</html>
