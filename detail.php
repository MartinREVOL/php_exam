<?php if ($_COOKIE['id'] == null) {
    header("Location: login.php");
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Détails de l'article</title>
  <link rel="stylesheet" type="text/css" href="detail.css">
</head>
<body>
  <div class="container">
    <?php
    include 'index.php';
      // Récupération des données de l'article à partir de la base de données
      $article_id = $_GET['article_id'];
      $user_id = $_COOKIE['id'];
      
      $sql = "SELECT * ,nbr_article FROM `article` INNER JOIN stock on article.article_id = stock.article_id WHERE article.article_id = $article_id;";
      $result = mysqli_query($conn, $sql);
      if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);

        $article_name = $row["name"];
        $article_image = $row["image_link"];
        $article_price = $row["cost"];
        $article_stock = $row["nbr_article"];
        $article_description = $row["description"];
        $article_date = $row["pub_date"];
      } else {
        echo "Aucun article trouvé avec l'ID $article_id";
      }

      $sqlusername = "SELECT username FROM `user`;";
      $result = mysqli_query($conn, $sqlusername);
      if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        
        $seller_name = $row["username"];
      } else {
        echo "Aucun user name trouver";
      }
      mysqli_close($conn);
    ?>

    <h1><?php echo $article_name; ?></h1>
    <img src="<?php echo $article_image; ?>" alt="<?php echo $article_name; ?>">
    <p>Prix : <?php echo $article_price; ?></p>
    <p>Stock : <?php echo $article_stock; ?></p>
    <p>Description : <?php echo $article_description; ?></p>
    <p>Date de publication : <?php echo $article_date; ?></p>
    <p>Nom du vendeur : <?php echo $seller_name; ?></p>

    <!-- Formulaire pour ajouter l'article au panier -->
    <form method="POST" action="add_cart2.php">
      <input type="hidden" name="article_id" value="<?php echo $article_id; ?>">
      <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
      <input type="submit" value="Ajouter au panier" class="form-submit">
    </form>
    <button onclick="window.history.back();" class="form-submit">Retour</button>
  </div>
</body>
</html>
