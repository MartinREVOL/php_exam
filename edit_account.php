<?php
include 'index.php';

// récupérer les informations de l'utilisateur actuel
$users_id = $_COOKIE['id'];
$query = "SELECT * FROM user WHERE user_id = $users_id";
$result = mysqli_query($conn, $query);
$user = mysqli_fetch_assoc($result);

if ($_POST) {
    // récupérer les informations envoyées par le formulaire
    $image = $_POST["image"];
    $username = $_POST["username"];
    $password1 = $_POST["password1"];
    $password2 = $_POST["password2"];

    if ($password1 !== $password2) {
        echo "The passwords do not match.";
    } else {
        $password = password_hash($password1, PASSWORD_DEFAULT);

        // mettre à jour les informations de l'utilisateur dans la base de données
        $query = "UPDATE user SET pp = '$image', username = '$username', mdp = '$password' WHERE user_id = '$users_id'";
        if (mysqli_query($conn, $query)) {
            header("Location: home.php");
            exit();
        } else {
            echo "Erreur d'enregistrement: " . mysqli_error($conn);
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="index.css">
    <title>Inscription</title>
</head>
<body>

<!-- Page de connexion -->
<div id="login">
    <h2>Modifier le profil</h2>
    <form action="" method="post">
        <label for="image">Image :</label>
        <input type="url" id="image" name="image" required value="<?php echo $user['pp']; ?>">

        <label for="username">Username :</label>
        <input type="text" id="username" name="username" required value="<?php echo $user['username']; ?>">

        <label for="password1">Password :</label>
        <input type="password" id="password1" name="password1" placeholder="New password" value="<?php $user['mdp']; ?>">

        <label for="password2">Confirmer le password :</label>
        <input type="password" id="password2" name="password2" placeholder="Confirme new password" value="<?php $user['mdp']; ?>">

        <div id="error-message" style="display: none; color: red;">The passwords do not match. Please enter the same password in both fields.</div>
        <br>
        <input type="submit" value="Done">
    </form>
    <a href="account.php">Back</a>
</div>

</body>
</html>

