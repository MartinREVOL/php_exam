<?php
session_start();
include 'index.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);

    $sql = "SELECT * FROM user WHERE e_mail = '$email'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $stored_password = $row['mdp'];

        if (password_verify($password, $stored_password)) {
            // Login successful, redirect to another page
            $id = $row['user_id'];
            setcookie('id', $id, time() + (60 * 60));
            header("Location: home.php");
            exit();
        } else {
            // Login failed, display an error message
            echo "Incorrect email or password, please try again.";
        }
    } else {
        // Login failed, display an error message
        echo "Incorrect email or password, please try again.";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="index.css">
  <title>Connexion</title>
</head>
<body>
<!-- Page de connexion -->
<div id="login">
  <h2>Login</h2>
  <form action="" method="post">
    <label for="email">E-mail :</label>
    <input type="email" id="email" name="email" required>
    <label for="password">Password :</label>
    <input type="password" id="password" name="password" required>

    <input type="submit" value="Done">
  </form>
  <p>Dont have any account ? <a href="register.php">Sign in</a></p>
</div>
</body>
</html>
