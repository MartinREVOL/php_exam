<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="sell.css">
    <title>Création d'un nouvel article</title>
  </head>
  <body>
    <div class="form-container">
      <h2>Création d'un nouvel article</h2>
      <form action="#" method="post">
        <input type="text" name="product_name" placeholder="Nom du produit" class="form-input">
        <textarea name="product_description" placeholder="Description du produit" class="form-input"></textarea>
        <input type="text" name="product_price" placeholder="Prix du produit" class="form-input">
        <input type="text" name="product_image" placeholder="Lien de l'image" class="form-input">
        <input type="text" name="product_stock" placeholder="Nombre d'articles en stock" class="form-input">
        <input type="submit" value="Créer l'article" class="form-submit">
      </form>
    </div>
  </body>
</html>
<?php
include 'index.php';

// Récupérer les données du formulaire
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
$product_name = mysqli_real_escape_string($conn,$_POST['product_name']);
$product_description = mysqli_real_escape_string($conn,$_POST['product_description']);
$product_price = mysqli_real_escape_string ($conn,$_POST['product_price']);
$product_image = mysqli_real_escape_string($conn,$_POST['product_image']);
$product_stock = mysqli_real_escape_string($conn,$_POST['product_stock']);
$aut_identity = $_COOKIE['id'];

// Préparer la requête d'insertion pour la table article
$sql1 = "INSERT INTO article (name, description, cost, pub_date, image_link, aut_id)
VALUES ('$product_name', '$product_description', '$product_price', (SELECT NOW()), '$product_image', '$aut_identity')";

// Exécuter la requête d'insertion pour la table article
if (mysqli_query($conn, $sql1)) {
    $article_id = mysqli_insert_id($conn);
    echo "Données de l'article ajoutées avec succès.";
} else {
    echo "Error: " . $sql1 . "<br>" . mysqli_error($conn);
}

// Préparer la requête d'insertion pour la table stock
$sql2 = "INSERT INTO stock (article_id, nbr_article)
VALUES ('$article_id', '$product_stock')";

// Exécuter la requête d'insertion pour la table stock
if (mysqli_query($conn, $sql2)) {
    echo "Données du stock ajoutées avec succès.";
    header("Location: articles.php");
    exit();
} else {
    echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
}
}
?>
