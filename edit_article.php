<?php

include 'index.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // Récupérer les données du formulaire
    $article_id = mysqli_real_escape_string($conn, $_POST['article_id']);
    $product_name = mysqli_real_escape_string($conn,$_POST['product_name']);
    $product_description = mysqli_real_escape_string($conn,$_POST['product_description']);
    $product_price = mysqli_real_escape_string ($conn,$_POST['product_price']);
    $product_image = mysqli_real_escape_string($conn,$_POST['product_image']);
    $product_stock = mysqli_real_escape_string($conn,$_POST['product_stock']);

    // Préparer la requête de mise à jour pour la table article
    $sql1 = "UPDATE article SET name='$product_name', description='$product_description', cost='$product_price', image_link='$product_image' WHERE article_id='$article_id'";

    // Exécuter la requête de mise à jour pour la table article
    if (mysqli_query($conn, $sql1)) {
        echo "Données de l'article mises à jour avec succès.";
    } else {
        echo "Error: " . $sql1 . "<br>" . mysqli_error($conn);
    }

    // Préparer la requête de mise à jour pour la table stock
    $sql2 = "UPDATE stock SET nbr_article='$product_stock' WHERE article_id='$article_id'";

    // Exécuter la requête de mise à jour pour la table stock
    if (mysqli_query($conn, $sql2)) {
        echo "Données du stock mises à jour avec succès.";
    } else {
        echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
    }
}

mysqli_close($conn);
// Rediriger l'utilisateur vers la page "articles.php"
header("Location: articles.php");
exit();

?>
