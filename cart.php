<?php if ($_COOKIE['id'] == null) {
    header("Location: login.php");
    exit();
}
?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Cart</title>
    <link rel="stylesheet" type="text/css" href="cart.css">
</head>
<body>
      <!-- Navbar -->
  <div id="navbar">
  <a href="home.php">
    <i class="fa fa-fw fa-shopping-basket"></i>
    Accueil
  </a>
  <a href="articles.php">
    <i class="fa fa-fw fa-shopping-cart"></i>
    Articles
  </a>
  <div id="navbar-title">Panier</div>
  <a href="account.php">
    <i class="fa fa-fw fa-user"></i>
    Profil
  </a>
</div>


<div class="content">
        <h1>Mon panier</h1>
        <?php
            include 'index.php';
            $user_id = $_COOKIE['id'];
            $query = "SELECT cart.article_id as article_id, COUNT(cart.article_id) as nbr_article , article.cost as cost, article.name as name FROM cart JOIN article ON cart.article_id = article.article_id WHERE cart.user_id GROUP BY cart.article_id;";
            $query1 = "SELECT SUM(solde) as total FROM user WHERE user_id = $user_id ORDER BY user_id DESC LIMIT 1;";
            $query2 = "SELECT SUM(article.cost) as total FROM `cart` JOIN article ON cart.article_id = article.article_id GROUP BY cart.user_id;";
            $result = mysqli_query($conn, $query);
            $result1 = mysqli_query($conn, $query1);
            $result2 = mysqli_query($conn, $query2);
            $row1 = mysqli_fetch_assoc($result1);
            ?>
                <div class="solde">
                <h3 class="price">Solde: <?php echo $row1['total']; ?>€</h3>
                </div>
            <?php
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    ?>
                    <div class="article">
                      <div class="article_price">
                        <h3 class="name"><?php echo $row['name']; ?></h3>
                        <p class="price">Prix: <?php echo $row['cost']; ?>€</p>
                        <p class="Nbr_article">Exemplaire: <?php echo $row['nbr_article'];?></p>
                      </div>
                    </div> 
                    <?php
                    $article_id = $row['article_id'];
                    ?>
                            <form method="POST" action="add_cart.php">
                                <input type="hidden" name="article_id" value="<?php echo $article_id; ?>">
                                <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                                <input type="submit" value="Ajouter un exemplaire" class="form-submit">
                            </form>
                            <form method="POST" action="delete_cart.php">
                                <input type="hidden" name="article_id" value="<?php echo $article_id; ?>">
                                <input type="submit" value="Supprimer l'article" class="form-submit">
                            </form>

                        <?php
                    
                          }
                    $row2 = mysqli_fetch_assoc($result2);
                    ?>
                    <p class="total_panier">Total du panier: <?php echo $row2['total']; ?>€</p>
                    <?php
                    if ($row2['total'] <= $row1['total']) {
                        ?>
                        <div class="commande">
                        <h3><a href="validate.php">Passer Commande</a></h3>
                        </div>
                        <?php
                    } else {
                        ?>
                        <h3>Vous ne pouvez pas passer commande votre solde est insuffisant</h3>
                        <?php
                    }
            } else {
                echo "Aucun article dans le panier.";
            }
        ?>
    </div>
</body>

</html>