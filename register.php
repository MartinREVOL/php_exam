<?php
include 'index.php';

if ($_POST) {
    $email = $_POST["email"];
    $username = $_POST["username"];
    $password1 = $_POST["password1"];
    $password2 = $_POST["password2"];

    if ($password1 !== $password2) {
        echo "The passwords do not match.";
    } else {
        $password = password_hash($password1, PASSWORD_DEFAULT); // chiffre le mot de passe avec une fonction de hachage sécurisée
        $role = "user"; // par défaut, l'utilisateur obtient le rôle "user"

        if ($password1 === "admin") {
            $role = "admin"; // si le mot de passe est "admin", l'utilisateur obtient le rôle "admin"
        }

        $query = "SELECT * FROM user WHERE e_mail='$email'";
        $result = mysqli_query($conn, $query);

        if (mysqli_num_rows($result) > 0) {
            echo 'An account with this email already exists. Please login.';
        } else {
            $query = "INSERT INTO user (e_mail, username, mdp, solde, pp, role) VALUES ('$email', '$username', '$password', '100', 'https://thumbs.dreamstime.com/b/image-neutre-de-profil-asexuelle-anonyme-moderne-avatar-monochrome-gradient-ic%C3%B4ne-abstraite-d-intelligence-artificielle-136072582.jpg', '$role')";
            if (mysqli_query($conn, $query)) {
                $ui = "SELECT user_id FROM user WHERE e_mail = '$email' AND mdp = '$password'";
                $resulta = mysqli_query($conn, $ui);
                if (mysqli_num_rows($resulta) > 0) {
                    $row = mysqli_fetch_assoc($resulta);
                }
                $id = $row['user_id'];
                setcookie('id', $id, time() + (60 * 60));
                header("Location: home.php");
                exit();
            } else {
                echo "Erreur d'enregistrement: " . mysqli_error($conn);
            }
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="index.css">
    <title>Inscription</title>
</head>
<body>

<!-- Page de connexion -->
<div id="login">
    <h2>Sign in</h2>
    <form action="" method="post">
        <label for="email">E-mail :</label>
        <input type="email" id="email" name="email" required>

        <label for="username">Username :</label>
        <input type="text" id="username" name="username" required>

        <label for="password">Password :</label>
        <input type="password" id="password1" name="password1" required>

        <label for="password">Confirme password :</label>
        <input type="password" id="password2" name="password2" required>
        <div id="error-message" style="display: none; color: red;">The passwords do not match. Please enter the same password in both fields.</div>
        <br>
        <input type="submit" value="Done">
    </form>
    <p>Already have an account ? <a href="login.php">Login</a></p>
</div>

</body>
</html>
