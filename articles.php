<?php if ($_COOKIE['id'] == null) {
    header("Location: login.php");
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="home.css">
  <title>Home</title>
</head>
<body>
  <!-- Navbar -->
  <div id="navbar">
  <a href="home.php">
    <i class="fa fa-fw fa-shopping-cart"></i>
    Accueil
  </a>
  <div id="navbar-title">Articles</div>
  <a href="cart.php">
    <i class="fa fa-fw fa-shopping-basket"></i>
    Panier
  </a>
  <a href="account.php">
    <i class="fa fa-fw fa-user"></i>
    Profil
  </a>
</div>


  <!-- Page Content -->
  <div class="content">
    <h1>Mes articles</h1>
    <?php
      include 'index.php';
      $sub = $_COOKIE['id'];
      $query1 = "SELECT * FROM article ORDER BY pub_date DESC";
      $query2 = "SELECT user.role FROM article INNER JOIN user ON article.aut_id = user.user_id WHERE article.aut_id = $sub GROUP BY `user`.`role`";
      $query = "SELECT * FROM article INNER JOIN user ON article.aut_id = user.user_id WHERE article.aut_id = $sub and user.role = 'user' ORDER BY pub_date DESC";
      $result2 = mysqli_query($conn, $query2);
      $result3 = mysqli_query($conn, $query1);
      $result = mysqli_query($conn, $query);
       if ($sub == 1){
          while ($row1 = mysqli_fetch_assoc($result3)) {
            ?>
            <div class="card" onclick="location.href='edit.php?article_id=<?php echo $row1['article_id']; ?>'">
              <img src="<?php echo $row1['image_link']; ?>" alt="Article Image">
              <div class="card-content">
                <h3><?php echo $row1['name']; ?></h3>
                <p class="price">Prix: <?php echo $row1['cost']; ?>€</p>
              </div>
            </div>
            <?php
           }
        } elseif (mysqli_num_rows($result) > 0) {
          while ($row = mysqli_fetch_assoc($result)) {
            ?>
              <div class="card" onclick="location.href='edit.php?article_id=<?php echo $row['article_id']; ?>'">
                <img src="<?php echo $row['image_link']; ?>" alt="Article Image">
                <div class="card-content">
                  <h3><?php echo $row['name']; ?></h3>
                  <p class="price">Prix: <?php echo $row['cost']; ?>€</p>
                </div>
              </div>
            <?php
          }
        } else {
            echo "Aucun article en vente pour le moment.";
        }
      ?>
    <a href="sell.php" class="button">+</a>
  </div>
</body>
</html>
