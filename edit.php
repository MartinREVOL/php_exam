<?php
include 'index.php';

$article_id = $_GET['article_id'];
      

      $sql = "SELECT * ,nbr_article FROM `article` INNER JOIN stock on article.article_id = stock.article_id WHERE article.article_id = $article_id;";
      $result = mysqli_query($conn, $sql);
      if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);

        $article_name = $row["name"];
        $article_image = $row["image_link"];
        $article_price = $row["cost"];
        $article_stock = $row["nbr_article"];
        $article_description = $row["description"];
        $article_date = $row["pub_date"];
        $seller_name = $row["aut_id"];
      } else {
        echo "Aucun article trouvé avec l'ID $article_id";
      }

      mysqli_close($conn);

// Récupérer les données du formulaire
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
$product_name = mysqli_real_escape_string($conn,$_POST['product_name']);
$product_description = mysqli_real_escape_string($conn,$_POST['product_description']);
$product_price = mysqli_real_escape_string ($conn,$_POST['product_price']);
$product_image = mysqli_real_escape_string($conn,$_POST['product_image']);
$product_stock = mysqli_real_escape_string($conn,$_POST['product_stock']);
$aut_identity = $_COOKIE['id'];

// Préparer la requête d'insertion pour la table article
$sql1 = "INSERT INTO article (name, description, cost, pub_date, image_link, aut_id)
VALUES ('$product_name', '$product_description', '$product_price', (SELECT NOW()), '$product_image', '$aut_identity')";

// Exécuter la requête d'insertion pour la table article
if (mysqli_query($conn, $sql1)) {
    $article_id = mysqli_insert_id($conn);
    echo "Données de l'article ajoutées avec succès.";
} else {
    echo "Error: " . $sql1 . "<br>" . mysqli_error($conn);
}

// Préparer la requête d'insertion pour la table stock
$sql2 = "INSERT INTO stock (article_id, nbr_article)
VALUES ('$article_id', '$product_stock')";

// Exécuter la requête d'insertion pour la table stock
if (mysqli_query($conn, $sql2)) {
    echo "Données du stock ajoutées avec succès.";
} else {
    echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Modifier l'article</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="edit.css">
</head>
<body>
<div class="form-container">
    <h1>Modifier l'article</h1>
    <form method="POST" action="edit_article.php" >
        <input type="hidden" name="article_id" value="<?php echo $article_id; ?>">
        <input type="text" name="product_name" value="<?php echo $article_name; ?>" class="form-input" required>
        <textarea name="product_description" class="form-input" required><?php echo $article_description; ?></textarea>
        <input type="text" name="product_price" value="<?php echo $article_price; ?>" class="form-input" required>
        <input type="text" name="product_image" value="<?php echo $article_image; ?>" class="form-input" required>
        <input type="text" name="product_stock" value="<?php echo $article_stock; ?>" class="form-input" required>
        <input type="submit" value="Modifier l'article" class="form-submit">
    </form>
        <form method="POST" action="delete_article.php">
            <input type="hidden" name="article_id" value="<?php echo $article_id; ?>">
            <input type="submit" value="Supprimer l'article" class="form-submit">
        </form>
    </div>
</body>
</html>

