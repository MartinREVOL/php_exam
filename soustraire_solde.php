<?php
// Connexion à la base de données
include 'index.php';

// Vérifier si le bouton '+' a été cliqué
if (isset($_POST['moins'])) {
  // Récupérer la valeur actuelle du solde pour l'utilisateur
  $user_id = $_COOKIE['id']; // Remplacez 1 par l'ID de l'utilisateur que vous voulez mettre à jour
  $result = mysqli_query($conn, "SELECT solde FROM user WHERE user_id = '$user_id'");
  $row = mysqli_fetch_assoc($result);
  $solde = $row['solde'];

  // Ajouter 10 à la valeur du solde
  $nouveau_solde = $solde - 10;

  // Mettre à jour la valeur du solde dans la base de données
  mysqli_query($conn, "UPDATE user SET solde=$nouveau_solde WHERE user_id=$user_id");
  header("Location: account.php");
  exit;
}
?>